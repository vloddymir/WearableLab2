package com.example.okaad.lab2;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

public class UserPreferences extends AppCompatActivity {
    private static final String DEFAULT_URL = "http://feeds.bbci.co.uk/news/world/europe/rss.xml";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_preferences);

        SharedPreferences preferences =
                getSharedPreferences(PreferenceHandler.PREF_PREFERENCES_RSS, 0);

        final String rssFeed = preferences.getString(PreferenceHandler.PREF_RSS_FEED, DEFAULT_URL);
        final int itemCountIndex = preferences.getInt(PreferenceHandler.PREF_ITEM_COUNT, 0);
        final int FetchRateIndex = preferences.getInt(PreferenceHandler.PREF_FETCH_RATE, 0);

        //------------------------------------------------------------------------------------------
        // RSS feed
        //------------------------------------------------------------------------------------------
        EditText rssFeedText = findViewById(R.id.edit_rss_feed);
        rssFeedText.setText(rssFeed);

        rssFeedText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String rssFeedText = editable.toString();

                SharedPreferences preference =
                        getSharedPreferences(PreferenceHandler.PREF_PREFERENCES_RSS, 0);

                SharedPreferences.Editor editor = preference.edit();
                editor.putString(PreferenceHandler.PREF_RSS_FEED, rssFeedText);
                editor.commit();
            }
        });

        //------------------------------------------------------------------------------------------
        // Spinner for item count
        //------------------------------------------------------------------------------------------
        Spinner spinnerItemCount = findViewById(R.id.spinner_item_count);

        ArrayAdapter<CharSequence> adapter = PreferenceHandler.getAdapterItemCount();

        spinnerItemCount.setAdapter(adapter);
        spinnerItemCount.setSelection(itemCountIndex);

        spinnerItemCount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SharedPreferences preference =
                        getSharedPreferences(PreferenceHandler.PREF_PREFERENCES_RSS, 0);

                SharedPreferences.Editor editor = preference.edit();
                editor.putInt(PreferenceHandler.PREF_ITEM_COUNT, i);
                editor.commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //------------------------------------------------------------------------------------------
        // Spinner for fetch rate
        //------------------------------------------------------------------------------------------
        Spinner spinnerFetchRate = findViewById(R.id.spinner_fetch_rate);

        adapter = PreferenceHandler.getAdapterFetchRate();

        spinnerFetchRate.setAdapter(adapter);
        spinnerFetchRate.setSelection(FetchRateIndex);

        spinnerFetchRate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SharedPreferences preference =
                        getSharedPreferences(PreferenceHandler.PREF_PREFERENCES_RSS, 0);

                SharedPreferences.Editor editor = preference.edit();
                editor.putInt(PreferenceHandler.PREF_FETCH_RATE, i);
                editor.commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
}
