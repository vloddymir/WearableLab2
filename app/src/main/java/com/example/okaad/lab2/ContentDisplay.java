package com.example.okaad.lab2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class ContentDisplay extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_display);

        Intent intent = getIntent();
        String articleUrlFromListDisplay = intent.getStringExtra(ListDisplay.ARTICLE_URL);
        WebView myWebView = findViewById(R.id.web_article_display);
        myWebView.loadUrl(articleUrlFromListDisplay);
    }
}
