package com.example.okaad.lab2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class ListDisplay extends AppCompatActivity {
    public static final String ARTICLE_URL = "URL";

    private TextView mTextViewRssFeed;
    private Button mButtonPreferences;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private ListView mListViewRss;
    private ArrayList<String> titles;
    private ArrayList<String> links;

    ScheduledExecutorService mScheduledRssFetcher;
    ScheduledFuture mFutureRssFetcher = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_display);

        mTextViewRssFeed = findViewById(R.id.text_rss_feed);
        mButtonPreferences = findViewById(R.id.button_preferences);
        mSwipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);

        mListViewRss = findViewById(R.id.list_items);
        titles = new ArrayList<>();
        links = new ArrayList<>();

        mScheduledRssFetcher = Executors.newSingleThreadScheduledExecutor();

        setUpPreferenceHandler();

        mButtonPreferences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity (new Intent (ListDisplay.this, UserPreferences.class));
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                scheduleRssFeedFetching();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        mListViewRss.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                Uri uri = Uri.parse(links.get(index));

                Intent intent = new Intent (ListDisplay.this, ContentDisplay.class);
                String message = uri.toString();
                intent.putExtra(ARTICLE_URL, message);
                startActivity(intent);
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        scheduleRssFeedFetching();
    }

    private void scheduleRssFeedFetching() {
        SharedPreferences preferences =
                getSharedPreferences(PreferenceHandler.PREF_PREFERENCES_RSS, 0);

        final int fetchRateIndex =
                preferences.getInt(PreferenceHandler.PREF_FETCH_RATE, 0);
        final int fetchRate = PreferenceHandler.getFetchRate(fetchRateIndex);

        if (mFutureRssFetcher != null) {
            mFutureRssFetcher.cancel(true);
        }

        mFutureRssFetcher = mScheduledRssFetcher.scheduleAtFixedRate(new RssFetcher(),
                0,
                fetchRate,
                TimeUnit.MILLISECONDS);
    }

    private void setUpPreferenceHandler(){
        SharedPreferences preferences =
                getSharedPreferences(PreferenceHandler.PREF_PREFERENCES_RSS, 0);

        final String rssFeed = preferences
                .getString(PreferenceHandler.PREF_RSS_FEED, PreferenceHandler.DEFAULT_URL);

        PreferenceHandler.setRssFeed(rssFeed);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.spinner_item_count_contents, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        PreferenceHandler.setAdapterItemCount(adapter);

        adapter = ArrayAdapter.createFromResource(this,
                R.array.spinner_fetch_rate_contents, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        PreferenceHandler.setAdapterFetchRate(adapter);
    }

    //  opens the web viewer for the provided URL (from RSS feed link)
    public InputStream getInputStream (URL url)
    {
        try
        {
            return url.openConnection().getInputStream();
        }
        catch (IOException e)
        {
            return null;
        }
    }

    private class RssFetcher implements Runnable {
        @Override
        public void run() {
            titles = new ArrayList<>();
            links = new ArrayList<>();
            String rssFeedTitle = "";
            try {
                SharedPreferences preferences =
                        getSharedPreferences(PreferenceHandler.PREF_PREFERENCES_RSS, 0);

                String savedUrl = preferences
                        .getString(PreferenceHandler.PREF_RSS_FEED, PreferenceHandler.DEFAULT_URL);
                final int itemCountIndex =
                        preferences.getInt(PreferenceHandler.PREF_ITEM_COUNT, 0);


                if (!savedUrl.startsWith("http://") && !savedUrl.startsWith("https://") &&
                        !savedUrl.startsWith("ht") &&!savedUrl.startsWith("http")) {
                    savedUrl = "http://" + savedUrl;
                }

                //check pattern for a valid URL pattern, and that the URL ends with .xml
                if (Patterns.WEB_URL.matcher(savedUrl).matches()) {

                    URL url = new URL(savedUrl);

                    XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(false);   //not support for XML namespaces

                    XmlPullParser xpp = factory.newPullParser();

                    try {
                        xpp.setInput(getInputStream(url), "UTF_8");


                        boolean insideItem = false;     //checking if we are currently inside an information item

                        int eventType = xpp.getEventType();
                        int numberOfItems = 0;
                        int desiredNumberOfItems = PreferenceHandler.getItemCount(itemCountIndex);


                        while (eventType != XmlPullParser.END_DOCUMENT) {
                            if (eventType == XmlPullParser.START_TAG && numberOfItems < desiredNumberOfItems) {
                                //get the title of the RSS feed site
                                if (xpp.getName().equalsIgnoreCase("title")) {
                                    if (!insideItem) {
                                        rssFeedTitle = xpp.nextText();
                                    } else {
                                        titles.add(xpp.nextText());
                                        numberOfItems++;
                                    }
                                }

                                if (xpp.getName().equalsIgnoreCase("item") || xpp.getName().equalsIgnoreCase("entry")) {
                                    insideItem = true;

                                } else if (xpp.getName().equalsIgnoreCase("link")) {
                                    if (insideItem) {
                                        links.add(xpp.nextText());
                                    }
                                }
                            }
                            else if (eventType == XmlPullParser.END_TAG && xpp.getName().equalsIgnoreCase("item")) {
                                insideItem = false;
                            }

                            eventType = xpp.next();
                        }
                    }
                    catch (XmlPullParserException e) {
                        Log.e("Fetch", e.getMessage());
                    }

                    if (savedUrl == null) {
                        Toast.makeText(ListDisplay.this,
                                "Please enter a valid RSS feed URL",
                                Toast.LENGTH_LONG).show();

                    }
                }
            }
            catch (MalformedURLException e)
            {
                Log.e("Fetch", e.getMessage());
            }
            catch (XmlPullParserException e)
            {
                Log.e("Fetch", e.getMessage());
            }
            catch (IOException e)
            {
                Log.e("Fetch", e.getMessage());
            }

            final String rssFeedTitleConfirmed = rssFeedTitle;
            final ArrayAdapter<String> adapter = new ArrayAdapter<>(ListDisplay.this, android.R
                    .layout.simple_list_item_1, titles);

            mTextViewRssFeed.post(new Runnable() {
                @Override
                public void run() {
                    mTextViewRssFeed.setText(rssFeedTitleConfirmed);
                }
            });

            mListViewRss.post(new Runnable() {
                @Override
                public void run() {
                    mListViewRss.setAdapter(adapter);
                }
            });
        }
    }
}
