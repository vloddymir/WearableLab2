package com.example.okaad.lab2;

import android.content.SharedPreferences;
import android.widget.ArrayAdapter;

/**
 * Created by okaad on 13.03.2018.
 */

public class PreferenceHandler {
    public static final String DEFAULT_URL = "http://feeds.bbci.co.uk/news/world/europe/rss.xml";
    public static final String PREF_PREFERENCES_RSS = "PREF_PREFERENCES_RSS";
    public static final String PREF_RSS_FEED = "PREF_RSS_FEED";
    public static final String PREF_ITEM_COUNT = "PREF_ITEM_COUNT";
    public static final String PREF_FETCH_RATE = "PREF_FETCH_RATE";

    private static String rssFeed = DEFAULT_URL;
    private static ArrayAdapter<CharSequence> adapterItemCount;
    private static ArrayAdapter<CharSequence> adapterFetchRate;

    public static String getRssFeed() {
        return rssFeed;
    }

    public static ArrayAdapter<CharSequence> getAdapterItemCount(){
        return adapterItemCount;
    }

    public static int getItemCount(int index) {
        return Integer.parseInt((adapterItemCount.getItem(index).toString()));
    }

    public static ArrayAdapter<CharSequence> getAdapterFetchRate() {
        return adapterFetchRate;
    }

    public static int getFetchRate(int index) {
        String fetchRate = adapterFetchRate.getItem(index).toString();

        // [0] will contain a number. [1] will contain the time unit (s, m, h)
        String[] timeUnits = fetchRate.split(" ");

        int timeUnitMultiplier = 1;

        if ("m".equals(timeUnits[1])){
            timeUnitMultiplier = 60;
        }
        if ("h".equals(timeUnits[1])){
            timeUnitMultiplier = 3600;
        }

        return 1000 * Integer.parseInt(timeUnits[0]) * timeUnitMultiplier;
    }

    public static void setRssFeed(String rssFeed) {
        PreferenceHandler.rssFeed = rssFeed;
    }

    public static void setAdapterItemCount(ArrayAdapter<CharSequence> adapterItemCount) {
        PreferenceHandler.adapterItemCount = adapterItemCount;
    }

    public static void setAdapterFetchRate(ArrayAdapter<CharSequence> adapterFetchRate) {
        PreferenceHandler.adapterFetchRate = adapterFetchRate;
    }
}
